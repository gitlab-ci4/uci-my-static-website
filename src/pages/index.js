import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <h1>static-website-ci - working with GitLab CI</h1>
    <h3>Descrition</h3>

    <ul>
      <li>This project was used by me to learn <b>GitLab CI: Pipelines, CI/CD and DevOps</b></li>
      <li>Using <b>.gitlab-ci.yml</b> to automate the build, test and deployment of a static website.<br />The Website can be seen here: <a href="http://cageyy-piesy.surge.sh/">http://cageyy-piesy.surge.sh/</a></li>
      <li>The Pipeline definition includes:</li>
        <ul>
          <li>build : using <b>Gatsby</b> to build the static website</li>
          <li>test : using <b>curl</b> requests to test</li>
          <li>deployment: using <b>surge</b> to deploy the website to a server with given url</li>
        </ul>
      <li>In this pipeline I use many dynamic Environments for the development and CI/CD process:</li>
        <ul>
          <li>Review/Test Environment</li>
          <li>Staging Environment (<a href="http://cageyy-piesy-staging.surge.sh/">http://cageyy-piesy-staging.surge.sh/</a>)</li>
          <li>Production Environment (<a href="http://cageyy-piesy.surge.sh/">http://cageyy-piesy.surge.sh/</a>)</li>
        </ul>
    </ul>

    <StaticImage
      src="../images/gatsby-astronaut.png"
      width={300}
      quality={95}
      formats={["auto", "webp", "avif"]}
      alt="A Gatsby astronaut"
      style={{ marginBottom: `1.45rem` }}
    />
  </Layout>
)

export default IndexPage
