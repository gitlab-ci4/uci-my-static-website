# static-website-ci - working with GitLab CI

## Descrition

- This project was used by me to learn  `GitLab CI: Pipelines, CI/CD and DevOps`


- Using `.gitlab-ci.yml` to automate the build, test and deployment of a static website.

  The Website can be seen here: http://cageyy-piesy.surge.sh/


- The Pipeline definition includes:
  - build : using `Gatsby` to build the static website
  - test : using `curl` requests to test
  - deployment: using `surge` to deploy the website to a server with given url


- In this pipeline I use many dynamic Environments for the development and CI/CD process:
  - Review/Test Environment
  - Staging Environment (http://cageyy-piesy-staging.surge.sh/)
  - Production Environment (http://cageyy-piesy.surge.sh/)
